# WEATHER APP - JS
---
A simple weather application that provides current weather information (weather condition, temperature, wind speed and humidity) for the city you choose to search for.

This version of app is basically the same as the previous one (Weather App) except that this one was built with JS AJAX to send the request.

## Built With
---
* HTML5
* CSS3
* SASS
* JavaScript
* [Ionicons](https://ionicons.com/v2/) - Icons
* [OpenWeatherMap.org](https://openweathermap.org) - Weather API