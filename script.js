

    document.getElementById('searchBtn').addEventListener('click', function() {
        // Get input value
        var city = document.getElementById('city').value;

        //Check if input is empty or not
        //If not empty
        if(city != '') {
            // send AJAX request
           loadInfo();
        } else {
        // If empty
            // Get input variable
            var input = document.getElementById('city');

            // Add 'error' class to input and change placeholder text
            input.classList.add('error');
            input.setAttribute('placeholder', 'Field cannot be empty');

            // On focus, remove 'error' class and change placeholder text to original value
            input.addEventListener('focus', function(){
                this.classList.remove('error');
                this.setAttribute('placeholder', 'Enter City Name');
            });
        }
    });

function loadInfo() {
    var request = new XMLHttpRequest();
    
    request.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=metric' +
    '&APPID=17fe5e86133e421df334e2b44d0cdd91', true );

    request.onload = function() {
        if(this.status === 200) {
            var responseData = JSON.parse(this.responseText);
            showData(responseData);
            document.getElementById('city').value = '';
        } 
    }

    request.send();
}    


function showData(data) {
    var cityName = data.name;
    var country = data.sys.country;
    var temp = Math.floor(data.main.temp);
    var wind = data.wind.speed;
    var humidity = data.main.humidity;
    var weather = data.weather[0].main;
    var icon = data.weather[0].icon;

    document.querySelector('.current-weather').innerHTML = weather;
    document.querySelector('.location').innerHTML = cityName + ', ' + country;
    document.querySelectorAll('.info')[0].innerHTML = temp + ' &deg;C';
    document.querySelectorAll('.info')[1].innerHTML = wind + ' m/s';
    document.querySelectorAll('.info')[2].innerHTML = humidity + ' %';
    document.querySelector('.icon').setAttribute('src', 'https://openweathermap.org/img/w/' + icon + '.png');
}